import "console"

rule TestCaseRule_OpenHarmony_SA_2023_0509
{
    meta:
        date = "2023-05-09"
        file = "/lib/system/libohosffmpeg.z.so"

    strings:
        $vul = {6b 4a 20 46 10 21 7a 44 a8 f1 ce ef 70 68 00 28}
        $fix = {00 f0 08 f9 70 68 00 28}

     condition:
        ((not $vul) or $fix) and console.log("OpenHarmony-SA-2023-0509 testcase pass")

}